#!/bin/env python3

from argparse import ArgumentParser
import json
import yaml
import logging

def parse_args():
    parser = ArgumentParser(description='Merges LOFAR SIP from MS')
    parser.add_argument('base_sip', help='base sip')
    parser.add_argument('sip', help='second sip')
    parser.add_argument('--json', help='output json file', default='')
    parser.add_argument('--yaml', help='output yaml file', default='')

    return parser.parse_args()


def load_sip(path):
    try:
        with open(path, 'r') as f_in:
            return json.load(f_in)
    except json.JSONDecodeError:
        pass
    try:
        with open(path, 'r') as f_in:
            return yaml.safe_load(f_in)
    except Exception as e:
        logging.exception(e)


def store(args, sip):
    if args.json:
        with open(args.json, 'w') as f_stream:
            json.dump(sip, f_stream, indent=True)
    elif args.yaml:
        with open(args.yaml, 'w') as f_stream:
            yaml.dump(sip, f_stream, sort_keys=False)
    else:
        print(yaml.dump(sip, sort_keys=False))


def merge(base, secondary):
    for key, value in secondary.items():
        if isinstance(value, dict) and key in base:
            merge(base[key], value)
        else:
            base[key] = value


def main():
    args = parse_args()
    base = load_sip(args.base_sip)
    values = load_sip(args.sip)
    merge(base, values)
    store(args, base)


if __name__ == '__main__':
    main()
