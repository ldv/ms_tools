#!/bin/env python3

from casacore.tables import table
from argparse import ArgumentParser
import json
import yaml
from astropy.time import Time
from datetime import timedelta
import isodate
import os

def parse_args():
    parser = ArgumentParser(description='Generates LOFAR SIP from MS')
    parser.add_argument('ms', help='input ms')
    parser.add_argument('--json', help='output json file', default='')
    parser.add_argument('--yaml', help='output json file', default='')

    return parser.parse_args()


def iter_col_values(table, cols, rownum=0):
    for col in cols:
        yield table.getcell(col, rownum)


def collect_project_info(msin):
    project = dict()
    obs_table = table(f'{msin}::OBSERVATION')
    project['projectCode'], \
       = iter_col_values(obs_table, (
        'PROJECT',
    ))
    return project

_storage_writer_to_storage_manager = {
    'DyscoStMan': 'DyscoStorageManager',
    'TiledColumnStMan': 'CasaStorageManager',
    'LofarStMan': 'LOFARStorageManager'
}

_DAYS_IN_SEC = 3600 * 24


def _mjs_to_isot_string(mjs):
    return Time(mjs / _DAYS_IN_SEC, format='mjd').to_value('isot')


def duration_from_start_end_mjs(start_mjs, end_mjs):
    start_time = Time(start_mjs / _DAYS_IN_SEC, format='mjd')
    end_time = Time(end_mjs / _DAYS_IN_SEC, format='mjd')
    duration: timedelta = (end_time - start_time).to_value('datetime')
    return isodate.duration_isoformat(duration)


def collect_dataproduct_info(msin):
    main_table = table(msin)
    spectral_window = table(f'{msin}::SPECTRAL_WINDOW')
    observation = table(f'{msin}::OBSERVATION')

    storage_manager = main_table.getdminfo('DATA')['TYPE']

    dataproduct = dict()
    dataproduct['dataProductType'] = 'Correlator data'
    dataproduct['_type'] = 'CorrelatedDataProduct'
    dataproduct['storageWriter'] = _storage_writer_to_storage_manager[storage_manager]
    dataproduct['storageWriterVersion'] = os.environ.get('DYSCO_COMMIT', 'None')
    dataproduct['subband'] = spectral_window.getcol('NAME', 0)[0].split('_')[-1]
    dataproduct['stationSubband'] = dataproduct['subband']

    obs_start_time = observation.getcell('LOFAR_OBSERVATION_START', 0)
    obs_end_time = observation.getcell('LOFAR_OBSERVATION_END', 0)

    start_time = _mjs_to_isot_string(obs_start_time)
    dataproduct['startTime'] = start_time
    dataproduct['duration'] = duration_from_start_end_mjs(obs_start_time, obs_end_time)
    dataproduct['integrationInterval'] = {
        '_attrs': {'units': 's'},
        '_value': float(main_table.getcell('INTERVAL', 0))
    }
    kHz_in_Hz = 1.e3
    dataproduct['centralFrequency'] = {
        '_attrs': {'units': 'MHz'},
        '_value': float(spectral_window.getcell('REF_FREQUENCY', 0))
    }

    dataproduct['channelWidth'] = {
        '_attrs': {'units': 'kHz'},
        '_value': float(spectral_window.getcell('CHAN_WIDTH', 0)[0] / kHz_in_Hz)
    }
    dataproduct['channelsPerSubband'] = spectral_window.getcell('NUM_CHAN', 0)
    return dataproduct


def store(args, sip):
    if args.json:
        with open(args.json, 'w') as f_stream:
            json.dump(sip, f_stream, indent=True)
    elif args.yaml:
        with open(args.yaml, 'w') as f_stream:
            yaml.dump(sip, f_stream, sort_keys=False)
    else:
        print(yaml.dump(sip, sort_keys=False))


def main():
    args = parse_args()

    sip = dict()
    sip['project'] = collect_project_info(args.ms)
    sip['dataProduct'] = collect_dataproduct_info(args.ms)
    store(args, sip)


if __name__ == '__main__':
    main()
