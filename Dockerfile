FROM astronrd/linc

COPY ./ /src
RUN cd /src && \
    python3 -m pip install .


RUN lofar_sip_from_ms.py --help && \
    lofar_sip_merge.py --help