import setuptools
import os
from os.path import join as path_join
import glob

repo_path = os.path.dirname(__file__)

README_PATH = path_join(repo_path, 'README.md')
REQUIREMENTS_PATH = path_join(repo_path, 'requirements.txt')

if os.path.exists(README_PATH):
    with open(README_PATH) as fh:
        long_description = fh.read()
else:
    long_description = ''

with open(REQUIREMENTS_PATH, 'r') as f_stream:
    requires = f_stream.read().splitlines()


scripts = glob.glob('bin/*.py')

setuptools.setup(
    name="ms-tools",
    version="0.0.1",
    author="Mattia Mancini",
    author_email="mancini@astron.nl",
    description="Tools for MeasurementSets handling",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.astron.nl/ldv/ms_tools",
    packages=setuptools.find_packages(),
    data_files = [],
    scripts=scripts,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache2 License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires = requires,
    test_suite='tests',
)
